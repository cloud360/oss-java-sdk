package com.cloudbase.sdk.s3.api;


import com.cloudbase.sdk.BaseAPI;
import com.cloudbase.sdk.http.Constants;
import com.cloudbase.sdk.s3.domain.Bucket;
import com.cloudbase.sdk.s3.domain.DownloadLink;
import com.cloudbase.sdk.s3.domain.File;
import com.cloudbase.sdk.s3.domain.Multipart;
import com.cloudbase.sdk.s3.domain.Part;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 12/23/14
 */
public class S3API extends BaseAPI {

    public final static String API_ENDPOINT = "http://s3.cloudbase.vn/api/v1";

    public S3API(String signature) {
        super(signature);
    }

    public S3API(String keyID, String keyValue) {
        super(keyID, keyValue);
    }

    public S3API(String keyID, String keyValue, String randomData) {
        super(keyID, keyValue, randomData);
    }

    @Override
    protected String getApiEndpoint() {
        return API_ENDPOINT;
    }

    public List<Bucket> listBuckets() {
        return doGet(Bucket.Buckets.class, "/buckets").buckets;
    }

    public void createBucket(String bucketName) {
        doPost(Void.class, "/buckets/" + bucketName, null);
    }

    public void deleteBucket(String bucketName) {
        doDelete(Void.class, "/buckets/" + bucketName);
    }

    public List<File> listFiles(String bucketName, String folder) {
        String path = "/buckets/" + bucketName + "?folder=" + (folder == null ? "/" : folder);
        return doGet(File.Files.class, path).files;
    }

    public void deleteFile(String bucketName, String filePath) {
        doDelete(Void.class, "/buckets/" + bucketName + "/" + filePath);
    }

    public DownloadLink upload(String bucketName, String filePath, InputStream in) {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/octet-stream");

        return doPut(DownloadLink.class, "/upload/" + bucketName + "/" + filePath, headers, in);
    }

    public Multipart initMultipart(String bucketName, String filePath, int numberOfParts) {
        String path = "/multipart/initiate/" + bucketName + "/" + filePath + "?numberOfParts=" + numberOfParts;
        return doPost(Multipart.class, path, null);
    }

    public Part uploadPart(String multipartID, InputStream in, int index) {
        String path = "/multipart/" + multipartID + "/part_upload?partIndex=" + index;
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/octet-stream");

        return doPut(Part.class, path, headers, in);
    }

    public List<Part> listParts(String multipartID) {
        String path = "/multipart/" + multipartID + "/parts";
        return doGet(Part.Parts.class, path).parts;
    }

    public DownloadLink completeMultipart(String multipartID) {
        return doPost(DownloadLink.class, "/multipart/" + multipartID + "/complete", null);
    }
}
