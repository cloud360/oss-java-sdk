package com.cloudbase.sdk.s3.domain;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 12/22/14
 */
public class Part {

    public String multipartId;

    public int partIndex;

    public long length;

    public Part() {
    }

    public class Parts {

        public List<Part> parts;

        public Parts() {
        }
    }
}
