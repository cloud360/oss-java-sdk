package com.cloudbase.sdk.s3.domain;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 12/22/14
 */
public class Multipart {

    public String bucket;

    public String filePath;

    public String fileName;

    public int numberOfParts;

    public Multipart() {
    }

}
