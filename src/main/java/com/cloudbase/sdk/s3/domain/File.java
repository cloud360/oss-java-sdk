package com.cloudbase.sdk.s3.domain;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 12/22/14
 */
public class File {

    public String bucket;

    public String path;

    public String downloadLink;

    public String directLink;

    public static class Files {

        public List<File> files;

        public Files() {
        }
    }
}
