package com.cloudbase.sdk.s3.domain;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 12/22/14
 */
public class Bucket {

    public String name;

    public String owner;

    public String shared;

    public Bucket() {
    }

    public static class Buckets {

        public Buckets() {
        }

        public List<Bucket> buckets;
    }
}
