package com.cloudbase.sdk;

import com.cloudbase.sdk.http.HttpCommand;
import com.cloudbase.sdk.http.HttpExecutor;
import com.cloudbase.sdk.http.HttpMethod;
import com.cloudbase.sdk.util.HmacSHA1;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 12/22/14
 */
public abstract class BaseAPI {

    protected String signature;

    protected HttpExecutor executor;

    public BaseAPI(String signature) {
        this.signature = signature;
    }

    public BaseAPI(String keyID, String keyValue) {
        this(keyID, keyValue, "abcdefgh");
    }

    public BaseAPI(String keyID, String keyValue, String randomData) {
        String data = keyID + ":" + randomData;
        try {
            String signedData = HmacSHA1.signature(data, keyValue);
            signature = data + ":" + signedData;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    protected abstract String getApiEndpoint();

    protected <R> R doGet(Class<R> resType, String path) {
        return doGet(resType, path, new HashMap<String, String>());
    }

    protected <R> R doGet(Class<R> resType, String path, Map<String, String> headers) {
        HttpCommand<R> c = HttpCommand.build(resType)
                .endpoint(getApiEndpoint())
                .path(path)
                .method(HttpMethod.GET)
                .signature(signature)
                .headers(headers)
                .build();

        return executor.execute(c, null);
    }

    protected final <R> R doPost(Class<R> resType, String path, Object entity) {
        return doPost(resType, path, new HashMap<String, String>(), entity);
    }

    protected final <R> R doPost(Class<R> resType, String path, Map<String, String> headers, Object entity) {
        HttpCommand<R> c = HttpCommand.build(resType)
                .endpoint(getApiEndpoint())
                .path(path)
                .method(HttpMethod.POST)
                .signature(signature)
                .headers(headers)
                .build();

        return executor.execute(c, entity);
    }

    protected final <R> R doDelete(Class<R> resType, String path, Map<String, String> headers) {
        HttpCommand<R> c = HttpCommand.build(resType)
                .endpoint(getApiEndpoint())
                .path(path)
                .method(HttpMethod.DELETE)
                .signature(signature)
                .headers(headers)
                .build();

        return executor.execute(c, null);
    }

    protected final <R> R doDelete(Class<R> resType, String path) {
        return doDelete(resType, path, new HashMap<String, String>());
    }

    protected final <R> R doPut(Class<R> resType, String path, Object entity) {
        return doPut(resType, path, new HashMap<String, String>(), entity);
    }

    protected final <R> R doPut(Class<R> resType, String path, Map<String, String> headers, Object entity) {
        HttpCommand<R> c = HttpCommand.build(resType)
                .endpoint(getApiEndpoint())
                .path(path)
                .method(HttpMethod.PUT)
                .signature(signature)
                .headers(headers)
                .build();

        return executor.execute(c, entity);
    }

}
