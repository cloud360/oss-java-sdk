package com.cloudbase.sdk.http;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 12/22/14
 */
public class Constants {

    public final static String AUTHORIZATION = "Authorization";

    public final static String CONTENT_TYPE = "Content-Type";

    public final static String ACCEPT = "Accept";
}
