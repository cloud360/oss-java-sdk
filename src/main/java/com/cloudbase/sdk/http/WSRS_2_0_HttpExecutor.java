package com.cloudbase.sdk.http;

import com.cloudbase.sdk.http.exception.ServerInternalException;
import com.cloudbase.sdk.http.exception.UnauthorizedException;
import org.glassfish.jersey.jackson.JacksonFeature;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 12/22/14
 */
public class WSRS_2_0_HttpExecutor implements HttpExecutor {

    private final Client wsClient;

    public WSRS_2_0_HttpExecutor(Client wsClient) {
        this.wsClient = wsClient;
        this.wsClient.register(JacksonFeature.class)
                .register(new JacksonMapperResolver());
    }

    @Override
    public final <R> R execute(HttpCommand<R> command, Object entity) {
        Response res = _execute(command, entity);

        //TODO: Check status and throw appropriate exceptions in non-happy cases
        int status = res.getStatus();

        if (status >= 400) {
            String message = res.readEntity(String.class);
            throw (status < 500) ? new UnauthorizedException(message) : new ServerInternalException(message);
        }

        return res.readEntity(command.resBodyType);
    }

    private <R> Response _execute(HttpCommand<R> command, Object entity) {
        WebTarget target = wsClient.target(command.endpoint);
        target = target.path(command.path);

        Invocation invocation = null;

        Invocation.Builder builder = target.request();
        if (command.signature != null) {
            builder = builder.header(Constants.AUTHORIZATION, command.signature);
        }

        builder = builder.accept(command.accept);

        for (Map.Entry<String, String> h : command.headers.entrySet()) {
            builder = builder.header(h.getKey(), h.getValue());
        }

        switch (command.method) {
            case GET:
                invocation = builder.buildGet();
                break;
            case DELETE:
                invocation = builder.buildDelete();
                break;
            case POST:
                builder = builder.header(Constants.CONTENT_TYPE, command.contentType);
                invocation = builder.buildPost(Entity.entity(entity == null ? "" : entity, command.contentType));
                break;
            case PUT:
                builder = builder.header(Constants.CONTENT_TYPE, command.contentType);
                invocation = builder.buildPut(Entity.entity(entity == null ? "" : entity, command.contentType));
                break;

        }

        return invocation.invoke();
    }

}
