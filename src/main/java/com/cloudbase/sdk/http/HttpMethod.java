package com.cloudbase.sdk.http;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 12/22/14
 */
public enum HttpMethod {

    GET,

    PUT,

    POST,

    DELETE
}
