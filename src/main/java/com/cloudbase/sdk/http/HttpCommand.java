package com.cloudbase.sdk.http;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 12/22/14
 */
public class HttpCommand<R> {

    String endpoint;

    String path;

    String signature;

    String contentType;

    String accept;

    HttpMethod method;

    Map<String, String> headers = new HashMap<>();

    Class<R> resBodyType;

    private HttpCommand(){
    }

    public static HttpCommandBuilder<Void> build(){
        return build(Void.class);
    }

    public static <R> HttpCommandBuilder<R> build(Class<R> resBodyType){
        return new HttpCommandBuilder<>(resBodyType);
    }

    public static class HttpCommandBuilder<R>{

        private String endpoint;

        private String path;

        private String signature;

        private String contentType = "application/json";

        private String accept = "application/json";

        private HttpMethod method;

        Map<String, String> headers = new HashMap<>();

        Class<R> resBodyType;

        private HttpCommandBuilder(Class<R> resBodyType){
            this.resBodyType = resBodyType;
        }

        public HttpCommandBuilder<R> endpoint(String endpoint){
            this.endpoint = endpoint;
            return this;
        }

        public HttpCommandBuilder<R> path(String path){
            this.path = path;
            return this;
        }

        public HttpCommandBuilder<R> signature(String signature){
            this.signature = signature;
            return this;
        }

        public HttpCommandBuilder<R> contentType(String contentType){
            this.contentType = contentType;
            return this;
        }

        public HttpCommandBuilder<R> accept(String accept){
            this.accept = accept;
            return this;
        }

        public HttpCommandBuilder<R> method(HttpMethod method){
            this.method = method;
            return this;
        }

        public HttpCommandBuilder<R> headers(Map<String, String> headers){
            this.headers.putAll(headers);
            return this;
        }

        public final HttpCommand<R> build(){
            HttpCommand<R> command = new HttpCommand<>();
            command.resBodyType = resBodyType;
            command.endpoint = endpoint;
            command.path = path;
            command.signature = signature;
            command.contentType = contentType;
            command.accept = accept;
            command.method = method;

            for(Map.Entry<String, String> h : headers.entrySet()){
                headers.put(h.getKey(), h.getValue());
            }

            return command;
        }
    }
}
