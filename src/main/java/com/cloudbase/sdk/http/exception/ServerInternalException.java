package com.cloudbase.sdk.http.exception;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 22/12/14
 */
public class ServerInternalException extends RuntimeException {

    public ServerInternalException(String message) {
        super(message);
    }
}
